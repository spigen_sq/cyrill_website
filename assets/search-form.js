$(function() {
    var currentAjaxRequest = null;
    var searchForms = $('form[action="/search"]').css('position', 'relative').each(function() {
        var input = $(this).find('input[name="q"]');
        input.attr('autocomplete', 'off').bind('keyup change', function() {
            var term = $(this).val();
            var form = $(this).closest('form');
            var searchURL = '/search?type=product&q=*' + term + '*';
            var resultsList = $('.search-results');
            resultsList.perfectScrollbar({
                suppressScrollX: true
            });
            if (term.length > 3 && term !== $(this).attr('data-old-term')) {
                $(this).attr('data-old-term', term);
                if (currentAjaxRequest !== null) currentAjaxRequest.abort();
                currentAjaxRequest = $.getJSON(searchURL + '&view=json', function(data) {
                    resultsList.empty();
                    if (data.results_count === 0) {
                        resultsList.html('<p>No results.</p>');
                        resultsList.fadeIn(200);
                        resultsList.hide();
                    } else {
                        $.each(data.results, function(index, item) {
                            var link = $('<a></a>').attr('href', item.url);
                            link.append('<span class="thumbnail"><img src="' + item.thumbnail + '" /></span>');
                            link.append('<span class="title">' + item.title + '</span>');
                            link.wrap('<div class="large-4 columns"></div>');
                            resultsList.append(link.parent());
                        });
                        if (data.results_count > 10) {
                            resultsList.append('<div class="row"><div class="semi-10 large-push-2 semi-push-1 large-8 columns"><a class="btn" href="' + searchURL + '"> +(' + data.results_count + ') more</a></div></div>');
                        }
                        resultsList.fadeIn(200);
                    }
                });
            }
        });
    });
    // Clicking outside makes the results disappear.
    //    $('body').bind('click', function(){
    //      $('.search-results').hide();
    //    });
});